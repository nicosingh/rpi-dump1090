[![pipeline status](https://gitlab.com/nicosingh/rpi-dump1090/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-dump1090/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-dump1090.svg)](https://hub.docker.com/r/nicosingh/rpi-dump1090/)

# About

Docker image of Raspbian with dump1090 decoder already installed. It is part of the following stack:

rpi-dump1090 -> **rpi-dump1090** -> rpi-fr24feed

Which has the purpose of configuring a homemade FlightRadar24 server using a Raspberry Pi and a DVB-T stick.

# How to use this Docker image?

As this image just installs dump1090 in Raspbian and does nothing else, probably the only way to use it is including this image in another Docker image :)

`FROM nicosingh/rpi-dump1090`
