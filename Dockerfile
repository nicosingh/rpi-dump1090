FROM nicosingh/rpi-rtl-sdr:latest

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

WORKDIR /

# Download dump1090 and install
RUN git clone https://github.com/MalcolmRobb/dump1090.git &&\
  cd /dump1090 &&\
  make &&\
  ln -s /dump1090 /bin/
